package notepaid;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;

public class FontFormat extends JDialog {
	private JLabel nameLb;
	private JLabel styleLb;
	private JLabel sizeLb;
	private JLabel presLb;
	private JTextField nameTx;
	private JTextField styleTx;
	private JTextField sizeTx;
	private JTextField presTx;
	private JList nameLt;
	private JList styleLt;
	private JList sizeLt;
	private JScrollPane jScrollPane1;
	private JScrollPane jScrollPane2;
	private JScrollPane jScrollPane3;
	private JButton approve;
	private JButton cancel;
	private JButton chose;
	private JRadioButton[] language = new JRadioButton[2];
	private ButtonGroup languageg;
	private String[] Slanguage = { new String("李涛"), new String("ABC") };
	private static JFrame frame;
	private static JTextArea jTextArea1;
	public Font font;
	public Font newFont;
	private Color color;
	Color newColor;
	private JFileChooser fileChoose = new JFileChooser();
	private JDialog colorDlg;
	private JColorChooser colorChoose = new JColorChooser();
	private GraphicsEnvironment environment;
	private String[] fontNameSet;
	private String[] fontStyleSet = { "常规", "倾斜", "加粗", "倾斜 加粗" };
	private Integer[] fontCon = { Integer.valueOf(0), Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(3) };
	private String[] fontSizeSet = { "6", "7", "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26",
			"28", "36", "48", "72" };

	public static void main(String[] args) {
		FontFormat a = new FontFormat();
		a.setVisible(true);
	}

	public FontFormat() {
		super(frame, "李涛—字体设置窗口", true);
		frame = new JFrame();
		initGUI();
	}

	public FontFormat(JFrame frame, JTextArea TextArea) {
		super(frame, "字体设置窗口", true);
		frame = frame;
		jTextArea1 = TextArea;

		initGUI();
	}

	private void initGUI() {
		try {
			getContentPane().setLayout(null);
			this.environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
			this.fontNameSet = this.environment.getAvailableFontFamilyNames();
			addMenu();
			initFont();

			setSize(450, 380);
			setDefaultCloseOperation(2);
			setWindowPos();
			setLocationRelativeTo(frame);
			setResizable(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initFont() {
		if (frame.getFont() == null) {
			this.nameTx.setText(this.fontNameSet[0]);
			this.styleTx.setText(this.fontStyleSet[0]);
			this.sizeTx.setText("12");
			this.nameLt.setSelectedValue(this.fontNameSet[0], true);
			this.styleLt.setSelectedIndex(0);
			this.sizeLt.setSelectedValue("12", true);
			this.font = new Font(this.fontNameSet[0], this.fontCon[0].intValue(), 12);
			this.newFont = this.font;
			this.presTx.setFont(this.font);
		} else {
			int idxStyle = 0;
			for (int i = 0; i < this.fontCon.length; i++) {
				if (this.fontCon[i].intValue() == frame.getFont().getStyle()) {
					idxStyle = i;
				}
			}
			this.nameTx.setText(frame.getFont().getName());
			this.styleTx.setText(this.fontStyleSet[idxStyle]);
			this.sizeTx.setText("" + frame.getFont().getSize());
			this.nameLt.setSelectedValue(frame.getFont().getName(), true);
			this.styleLt.setSelectedIndex(idxStyle);
			this.sizeLt.setSelectedValue("" + frame.getFont().getSize(), true);
			this.font = new Font(this.fontNameSet[0], this.fontCon[0].intValue(), 12);
			this.newFont = this.font;
			this.presTx.setFont(this.font);
		}
	}

	private void addMenu() {
		this.nameLb = new JLabel();
		getContentPane().add(this.nameLb);
		this.nameLb.setText("字体：");
		this.nameLb.setBounds(10, 14, 120, 26);
		this.nameLb.setFont(new Font("SimSun", 1, 14));

		this.styleLb = new JLabel();
		getContentPane().add(this.styleLb);
		this.styleLb.setText("字型：");
		this.styleLb.setBounds(151, 14, 120, 23);
		this.styleLb.setFont(new Font("SimSun", 1, 14));

		this.sizeLb = new JLabel();
		getContentPane().add(this.sizeLb);
		this.sizeLb.setText("大小：");
		this.sizeLb.setBounds(275, 14, 79, 24);
		this.sizeLb.setFont(new Font("SimSun", 1, 14));

		this.presLb = new JLabel();
		getContentPane().add(this.presLb);
		this.presLb.setText("预览：");
		this.presLb.setBounds(151, 150, 120, 80);
		this.presLb.setFont(new Font("SimSun", 1, 14));

		this.nameTx = new JTextField();
		this.nameTx.setEditable(false);
		getContentPane().add(this.nameTx);
		this.nameTx.setBounds(10, 42, 120, 22);

		this.styleTx = new JTextField();
		this.styleTx.setEditable(false);
		getContentPane().add(this.styleTx);
		this.styleTx.setBounds(151, 42, 100, 21);

		this.sizeTx = new JTextField();
		this.sizeTx.setEditable(false);
		getContentPane().add(this.sizeTx);
		this.sizeTx.setBounds(275, 42, 79, 22);

		this.presTx = new JTextField();
		this.presTx.setEditable(false);
		getContentPane().add(this.presTx);
		this.presTx.setBounds(151, 200, 203, 61);
		this.presTx.setText(this.Slanguage[1]);

		this.jScrollPane1 = new JScrollPane();
		getContentPane().add(this.jScrollPane1);
		this.jScrollPane1.setBounds(10, 74, 120, 210);

		ListModel fontNameModel = new DefaultComboBoxModel(this.fontNameSet);
		this.nameLt = new JList();
		this.jScrollPane1.setViewportView(this.nameLt);
		this.nameLt.setModel(fontNameModel);
		this.nameLt.setBounds(274, 193, 90, 86);
		this.nameLt.setBorder(BorderFactory.createEtchedBorder(1));
		this.nameLt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				FontFormat.this.nameLtMouseClicked(evt);
			}
		});
		this.jScrollPane2 = new JScrollPane();
		getContentPane().add(this.jScrollPane2);
		this.jScrollPane2.setBounds(151, 74, 100, 103);

		ListModel fontStyleModel = new DefaultComboBoxModel(this.fontStyleSet);
		this.styleLt = new JList();
		this.jScrollPane2.setViewportView(this.styleLt);
		this.styleLt.setModel(fontStyleModel);
		this.styleLt.setBounds(310, 215, 70, 102);
		this.styleLt.setBorder(BorderFactory.createEtchedBorder(1));
		this.styleLt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				FontFormat.this.styleLtMouseClicked(evt);
			}
		});
		this.jScrollPane3 = new JScrollPane();
		getContentPane().add(this.jScrollPane3);
		this.jScrollPane3.setBounds(275, 75, 79, 100);

		ListModel fontSizeModel = new DefaultComboBoxModel(this.fontSizeSet);
		this.sizeLt = new JList();
		this.jScrollPane3.setViewportView(this.sizeLt);
		this.sizeLt.setModel(fontSizeModel);
		this.sizeLt.setBounds(300, 218, 54, 102);
		this.sizeLt.setBorder(BorderFactory.createEtchedBorder(1));
		this.sizeLt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				FontFormat.this.sizeLtMouseClicked(evt);
			}
		});
		this.languageg = new ButtonGroup();
		this.language[0] = new JRadioButton("中");
		getContentPane().add(this.language[0]);
		this.language[0].setSelected(false);
		this.language[0].setBounds(271, 179, 40, 20);
		this.language[0].setFont(new Font("SimSun", 1, 12));
		this.languageg.add(this.language[0]);
		this.language[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FontFormat.this.presTx.setText(FontFormat.this.Slanguage[0]);
			}
		});
		this.language[1] = new JRadioButton("英");
		getContentPane().add(this.language[1]);
		this.language[1].setSelected(true);
		this.language[1].setBounds(321, 179, 40, 20);
		this.language[1].setFont(new Font("SimSun", 1, 12));
		this.languageg.add(this.language[1]);
		this.language[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FontFormat.this.presTx.setText(FontFormat.this.Slanguage[1]);
			}
		});
		this.approve = new JButton();
		getContentPane().add(this.approve);
		this.approve.setText("确定");
		this.approve.setBounds(151, 265, 67, 20);
		this.approve.setFont(new Font("KaiTi_GB2312", 1, 12));
		this.approve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FontFormat.this.approveActionPerformed(evt);
			}
		});
		this.cancel = new JButton();
		getContentPane().add(this.cancel);
		this.cancel.setText("取消");
		this.cancel.setBounds(219, 265, 67, 20);
		this.cancel.setFont(new Font("KaiTi_GB2312", 1, 12));
		this.cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FontFormat.this.cancelActionPerformed(evt);
			}
		});
		this.chose = new JButton();
		getContentPane().add(this.chose);
		this.chose.setText("颜色");
		this.chose.setBounds(287, 265, 67, 20);
		this.chose.setFont(new Font("KaiTi_GB2312", 1, 12));
		this.chose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FontFormat.this.choseActionPerformed(evt);
			}
		});
	}

	private void setWindowPos() {
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension frameSize = new Dimension();
		Dimension screenSize = kit.getScreenSize();
		getSize(frameSize);
		setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	private void nameLtMouseClicked(MouseEvent evt) {
		this.nameTx.setText(this.nameLt.getSelectedValue().toString());
		this.font = new Font(this.nameTx.getText(), this.font.getStyle(), this.font.getSize());
		this.presTx.setFont(this.font);
	}

	private void styleLtMouseClicked(MouseEvent evt) {
		String temp = this.styleLt.getSelectedValue().toString();
		this.styleTx.setText(temp);
		int index = 0;
		while ((index < 4) && (!this.fontStyleSet[index].equals(temp))) {
			index++;
		}
		this.font = new Font(this.font.getName(), this.fontCon[index].intValue(), this.font.getSize());
		this.presTx.setFont(this.font);
	}

	private void sizeLtMouseClicked(MouseEvent evt) {
		this.sizeTx.setText(this.sizeLt.getSelectedValue().toString());

		this.font = new Font(this.font.getName(), this.font.getStyle(), Integer.parseInt(this.sizeTx.getText()));
		this.presTx.setFont(this.font);
	}

	private void approveActionPerformed(ActionEvent evt) {
		String name = this.nameTx.getText();
		int style = this.fontCon[this.styleLt.getSelectedIndex()].intValue();
		int size = Integer.parseInt(this.sizeTx.getText());
		this.font = new Font(name, style, size);

		this.newFont = this.font;
		this.newColor = this.color;
		jTextArea1.setFont(this.font);
		jTextArea1.setCaretColor(this.color);
		dispose();
	}

	private void cancelActionPerformed(ActionEvent evt) {
		dispose();
	}

	private void choseActionPerformed(ActionEvent evt) {
		if (this.colorDlg == null) {
			this.colorDlg = JColorChooser.createDialog(this, "Select Text Color", true, this.colorChoose,
					new ColorOKListener(), null);
		}
		this.colorChoose.setColor(this.color = this.presTx.getForeground());
		this.colorDlg.setVisible(true);
	}

	class ColorOKListener implements ActionListener {
		ColorOKListener() {
		}

		public void actionPerformed(ActionEvent e) {
			Color c = FontFormat.this.colorChoose.getColor();
			FontFormat.this.color = c;
			FontFormat.this.presTx.setForeground(c);
			FontFormat.this.presTx.repaint();
		}
	}
}
