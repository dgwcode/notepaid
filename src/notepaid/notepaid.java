package notepaid;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

public class notepaid extends JFrame {
	private JMenuBar jMenuBar;
	private JMenuItem jMenuItemabort;
	private JMenuItem jMenuItemfont;
	private JMenuItem jMenuItmen;
	private JMenu jMenuabout;
	private JMenu jMenuedit;
	private JMenu jMenufile;
	private JScrollPane jScrollPane1;
	private JTextArea jTextArea1;

	public notepaid() throws Exception {
		initComponents();
	}

	private void initComponents() {
		this.jScrollPane1 = new JScrollPane();
		this.jTextArea1 = new JTextArea();
		this.jMenuBar = new JMenuBar();
		this.jMenufile = new JMenu();
		this.jMenuItmen = new JMenuItem();
		this.jMenuedit = new JMenu();
		this.jMenuItemfont = new JMenuItem();
		this.jMenuabout = new JMenu();
		this.jMenuItemabort = new JMenuItem();

		setDefaultCloseOperation(3);
		setTitle("JAVA记事本");

		this.jTextArea1.setColumns(20);
		this.jTextArea1.setLineWrap(true);
		this.jTextArea1.setRows(5);
		this.jScrollPane1.setViewportView(this.jTextArea1);

		this.jMenufile.setText("文件");

		this.jMenuItmen.setAccelerator(KeyStroke.getKeyStroke(83, 2));
		this.jMenuItmen.setText("保存");
		this.jMenuItmen.setToolTipText("");
		this.jMenuItmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				notepaid.this.jMenuItmenActionPerformed(evt);
			}
		});
		this.jMenufile.add(this.jMenuItmen);

		this.jMenuBar.add(this.jMenufile);

		this.jMenuedit.setText("编辑");

		this.jMenuItemfont.setText("font");
		this.jMenuItemfont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				notepaid.this.jMenuItemfontActionPerformed(evt);
			}
		});
		this.jMenuedit.add(this.jMenuItemfont);

		this.jMenuBar.add(this.jMenuedit);

		this.jMenuabout.setText("关于");
		this.jMenuabout.setToolTipText("");

		this.jMenuItemabort.setText("关于");
		this.jMenuItemabort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				notepaid.this.jMenuItemabortActionPerformed(evt);
			}
		});
		this.jMenuabout.add(this.jMenuItemabort);

		this.jMenuBar.add(this.jMenuabout);

		setJMenuBar(this.jMenuBar);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(this.jScrollPane1, -1, 800, 32767));

		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(this.jScrollPane1, -1, 429, 32767));

		pack();
	}

	private void jMenuItemabortActionPerformed(ActionEvent evt) {
		JOptionPane.showMessageDialog(this.rootPane, "基于beautyeye_lnf框架 开发的记事本   by guowei", "关于", 2);
	}

	private void jMenuItmenActionPerformed(ActionEvent evt) {
		try {
			new saveText(this.jTextArea1);
		} catch (IOException ex) {
			Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void jMenuItemfontActionPerformed(ActionEvent evt) {
		FontFormat a = new FontFormat(this, this.jTextArea1);
		a.setVisible(true);
	}

	public static void main(String[] args)
  {
    try
    {
      for (UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName()))
        {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (ClassNotFoundException ex)
    {
      Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (InstantiationException ex)
    {
      Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (UnsupportedLookAndFeelException ex)
    {
      Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
    }
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          BeautyEyeLNFHelper.launchBeautyEyeLNF();
          new notepaid().setVisible(true);
        }
        catch (Exception ex)
        {
          Logger.getLogger(notepaid.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    });
  }

	private static class saveText {
		public saveText(JTextArea jTextArea1) throws IOException {
			JFileChooser file = new JFileChooser(System.getProperty("user.dir"));
			file.setFileFilter(new myfilefilter());
			file.showSaveDialog(null);
			if (jTextArea1.getText() != null) {
				File fi = file.getSelectedFile();
				System.out.println(fi.getAbsolutePath() + ".txt");
				FileWriter out = new FileWriter(fi.getAbsolutePath() + ".txt");
				Throwable localThrowable3 = null;
				try {
					out.write(jTextArea1.getText());
					out.flush();
					out.close();
				} catch (Throwable localThrowable1) {
					localThrowable3 = localThrowable1;
					throw localThrowable1;
				} finally {
					if (out != null) {
						if (localThrowable3 != null) {
							try {
								out.close();
							} catch (Throwable localThrowable2) {
								localThrowable3.addSuppressed(localThrowable2);
							}
						} else {
							out.close();
						}
					}
				}
			}
		}
	}
}
