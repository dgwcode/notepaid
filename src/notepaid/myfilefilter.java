package notepaid;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class myfilefilter extends FileFilter {
	public boolean accept(File f) {
		if (f.getName().endsWith(".txt") == true) {
			return true;
		}
		return false;
	}

	public String getDescription() {
		return "*.txt";
	}
}
